@extends('master')

  <!-- Content Wrapper. Contains page content -->

    @section('content')
    <div class="row">
      <<div class="col-12 text-center">
        <h3>User Table Having Relation</h3>
      </div>
    </div>
    <div class="row my-5">
      <<div class="col-1">
      </div>
    <div class="row-10 m-auto">
      <table class="table table-dark container-fluid">
    
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
     



          
        </tr>
      </thead>
      <tbody>
    
        @foreach($user as $item)
        <tr>
          <th scope="row">{{$item->id}}</th>
          <td>{{$item->name}}</td>
          <td>{{$item->email}}</td>
          {{-- <td>{{$item->mypost ? $item->mypost->type: '' }}</td>
          <td>{{$item->mypost ? $item->mypost->catagory : '' }}</td> --}}
       


        </tr>
        @endforeach
      </tbody>
    </table>
 
    </div>  
    <div class="col-1">
      </div>
    </div>
    

@endsection
@section('scripts')
<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

@endsection
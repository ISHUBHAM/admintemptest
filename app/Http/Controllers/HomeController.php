<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{


     public function __construct()
     {
         $this->middleware('cauth');
 
         
     }
 
      public function getHomepage()
      {

           return view('homepage');
      }
      public function getDashboard1()
      {

           return view('index1');
      }

      public function getDashboard2()
      {

           return view('index2');
      }
      public function getDashboard3()
      {

           return view('index3');
      }

      public function   widgate()
      {
           return view('widgate');

      }

  



}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LayoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('cauth'); 

    }
    public function topNav()
    {
        return view('addnav.topnav');
    }

    public function topnavSidebar()
    {
        return view('addnav.topnavsidebar');

    }

    public function boxed()
    {
        return view('addnav.boxed');

    }
    public function fixedSidebar()
    {
        return view('addnav.fixedsidebar');

    }
    public function fixedNavbar()
    {
        return view('addnav.fixedtopnav');

    }
    public function fixedFooter()
    {
        return view('addnav.fixedfooter');

    }

    public function collapsedSidebar()
    {
        return view('addnav.collapsedsidebar');

    }


}

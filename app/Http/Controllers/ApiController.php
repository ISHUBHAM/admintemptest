<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Profile;
use DB;
use PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
class ApiController extends Controller
{



  public function loginApi($email,$password)
  {
      if(Auth::attempt(['email'=>$email,'password'=>$password]))

      {
               return 'login successful';

      }
      else
      {
        return 'incorrect userid or password';

      }


  }

  public function registerApi(Request $req)

  {
        $data = new User;
    	$data->name=$req->name;
    	$data->email=$req->email;
    	$data->password=bcrypt($req->password);
    	$data->save();
    	return "Register Sucessfull";

  }







    
    public function allUser()
    {

        // API 1 for get the all users table data 
        $user = DB::table('users')
        ->select('name','email','id')
        ->get();
        return $user;

    }

    public function allPost()
    {
        // API 2 for get the all post table data
        $data = DB::table('posts')
		->join('users', 'users.id', '=', 'posts.user_id')
		->select('users.*', 'posts.*')
		->get();
		return $data;


    }

    public function specificData($post_id)
	{
        //API 3 for get the specific data from users table through through post table user_id


        $data=DB::table('users')
        ->join('posts', 'users.id', '=', 'posts.user_id')
		->select('users.*','posts.*')
		->where('users.id',$post_id)
		->get();
		return response()->json($data);
    }


    public function update($id)
    {
        // API 4 for update the posts table type column by update the record
        $data=DB::table('posts')
        ->where('posts.user_id',$id)
    	->update(['type' => 'update the record']);
        return response()->json($data);
        
    }

    public function delete($id)
    {
        // API  5 for delete the posts table id by passing a id

    	DB::table('posts')
    	->join('users', 'users.id', '=', 'posts.user_id')
    	->where('posts.id','=',$id)
    	->delete();
    	return 'deleted';
    }

    public function betweendate()
    {
        // API  6 to get between any  two  date all data in the posts table 

    	$alldata=DB::table('posts')
    	->whereBetween('created_at',['20-08-18','20-08-20'])
    	->get();
    	return response()->json($alldata);

    }

    public function userhavepost()
    {
        // Api 7 to get any data having the same id in the table Fatch  the data 

    	$data = DB::table('posts')
    	->join('users', 'users.id', '=', 'posts.user_id')
    	->select('users.*')
    	->get();
        return response()->json($data);
    }




    public function posthavedate($id)

    {
        // Get posts of users having any specific user_id (say 13), between any specific date and get only two columns, Post's name and Post's date. 
        
        $data=DB::table('posts')
        ->join('users','users.id','=','posts.user_id')
        ->select('posts.type','posts.catagory')
        ->where('posts.user_id','=',$id)
        ->whereBetween('posts.created_at',['2020-08-18','2020-09-29'])
        ->get();
        return response()->json($data);

    }



    public function pdfsend($check)
    {
       
    //    Get the post data and send  the data with a attachment file through mail in the form of PDF
       
        $data=DB::table('posts')
            ->join('users','users.id','=','posts.user_id')
            ->select('users.*','posts.*')
            ->where('posts.user_id','=',$check)
            ->get();

            $arr["email"]='hellomail@gmail.com';
            $arr["name"]='shubham';
            $arr["sub"]='All Post data';


            $final = PDF::loadView('pdfdata',compact('data'));
            $final=$final->output();

            Mail::send([], $arr, function($message)use($arr,$final) {
                $message->to($arr["email"], $arr["name"])
                ->subject($arr["sub"])
                ->attachData($final, 'postsdata.pdf',
                 [
                    'mime' => 'application/pdf',
                ])
                ->setBody('Hi, welcome user!');

                });


    }


    
}

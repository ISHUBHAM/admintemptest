<?php

namespace App\Listeners;

use App\Events\MailSentEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use App\Jobs\SendEmailJob;



class MailSentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MailSentEvent  $event
     * @return void
     */
    public function handle(MailSentEvent $event)
    {
                        // Mail::to($event->user)->send(new WelcomeMail());

                        SendEmailJob::dispatch()
                        ->delay(now()->addSeconds(10));
        
    }
}
